#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "board.h"
#include "search.h"

#define MAX_UNDO_STEPS 4

int main(int argc, char *argv[])
{
    struct search *search = search__alloc(NULL);

    const size_t board_size = board__size();

    char *undo_boards = malloc(MAX_UNDO_STEPS * board_size);
#define UNDO_BOARD(n) ((struct board *) (undo_boards + board_size * (n)))
    int undo_steps = 0;

    for (;;)
    {
        struct board *const board = search__board(search);
    
        board__print_board(stdout, board);
        printf("\n");

        const bool game_over = board__next_move(board, -1) < 0;

        int move;

        char buf[16];
        if (fgets(buf, sizeof buf, stdin) == NULL)
            return ferror(stdin);
        if (buf[strlen(buf) - 1] != '\n')
            return 1;

        if (strcmp(buf, "undo\n") == 0 || strcmp(buf, "u\n") == 0)
        {
            if (undo_steps < 1)
            {
                printf("Can't undo!\n\n");
                continue;
            }

            search__free(search);
            search = search__alloc(memcpy(malloc(board__size()), UNDO_BOARD(0), board__size()));

            memmove(UNDO_BOARD(0), UNDO_BOARD(1), board_size * (undo_steps - 1));
            undo_steps--;

            continue;
        }

        if (game_over)
        {
            printf("Game over!\n\n");
            break;
        }

        if (strcmp(buf, "go\n") == 0 || strcmp(buf, "g\n") == 0)
        {
            search__search(search, 3.0);
            move = search__best_move(search);

            printf("Chose: ");
            board__print_move(stdout, move);
        }
        else
        {
            move = board__parse_move(buf);

            if (move < 0)
            {
                printf("Invalid move!\n\n");
                continue;
            }

            if (!board__move_is_legal(board, move))
            {
                printf("Illegal move!\n\n");
                continue;
            }
        }

        printf("Win chance: %f\n\n", search__win_chance(search, move));

        if (undo_steps >= MAX_UNDO_STEPS) undo_steps--;
        memmove(UNDO_BOARD(1), UNDO_BOARD(0), board_size * undo_steps);
        undo_steps++;
        memcpy(UNDO_BOARD(0), search__board(search), board_size);

        search__step(search, move);
    }

    free(undo_boards);

    search__free(search);

    return 0;
}

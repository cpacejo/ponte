#ifndef BOARD_H
#define BOARD_H

#include <stdbool.h>
#include <stddef.h>

struct board;

size_t board__size(void);

void board__reset(struct board *);
int board__players(const struct board *);

int board__winner(struct board *);  // -1 -> tie

int board__current_player(const struct board *);
int board__next_move(struct board *, int move);  // -1 -> start/end; no moves = game end
int board__random_move(struct board *, long int random);
void board__apply_move(struct board *, int move);

bool board__move_is_legal(struct board *, int move);

#include <stdio.h>

int board__print_board(FILE *file, struct board *);
int board__print_move(FILE *file, int move);
int board__parse_move(const char *line);  // -1 -> invalid

#endif

#include <assert.h>
#include <math.h>
#include <omp.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "board.h"
#include "search.h"

#define EXPLORATION_COEFFICIENT M_SQRT1_2

#define PARALLELISATION 16

// FIXME: score ties correctly

struct tree
{
    int move;
    int num_children;
    bool complete;
    // chance that the player who made this move wins
    long int visits, wins;
    struct tree *children;
};

struct search
{
    struct tree root[PARALLELISATION];
    unsigned int seed[PARALLELISATION];
    struct board *board;
};

static void free_tree(struct tree *const tree)
{
    for (int i = 0; i < tree->num_children; i++)
        free_tree(&tree->children[i]);
    free(tree->children);
}

static float child_score(const struct tree *const tree, const float c_sqrt_2_n)
{
    return (float) tree->wins / (float) tree->visits + c_sqrt_2_n / sqrtf(tree->visits);
}

static int best_child(const struct tree *const tree,
    const float exploration_coefficient)
{
    assert(tree->num_children > 0);

    const float c_sqrt_2_n = (2.0f * exploration_coefficient) * sqrtf(logf(tree->visits));

    int best = 0;
    float score = 0.0f;

    for (int i = 0; i < tree->num_children; i++)
    {
        const float new_score = child_score(&tree->children[i], c_sqrt_2_n);
        if (new_score > score)
        {
            best = i;
            score = new_score;
        }
    }

    return best;
}

static int expand(struct board *const board, struct tree *const tree)
{
    const int next_move = board__next_move(board,
        tree->num_children == 0 ? -1 :
            tree->children[tree->num_children - 1].move);

    if (next_move == -1)
    {
        tree->complete = true;
        return -1;
    }
    else
    {
        tree->num_children++;
        tree->children = realloc(tree->children,
            sizeof tree->children[0] * tree->num_children);

        struct tree *const child = &tree->children[tree->num_children - 1];
        child->move = next_move;
        child->num_children = 0;
        child->complete = false;
        child->visits = 0;
        child->wins = 0;
        child->children = NULL;
        return tree->num_children - 1;
    }
}

static int random_move(struct board *const board, unsigned int *const seed)
{
    return board__random_move(board, rand_r(seed));
}

static int tree_policy(struct board *const board, struct tree *const tree)
{
    if (tree->complete)
    {
        if (tree->num_children == 0) return -1;
        else return best_child(tree, EXPLORATION_COEFFICIENT);
    }
    else
    {
        const int child = expand(board, tree);
        if (child < 0)
        {
            assert(tree->complete);
            if (tree->num_children == 0) return -1;
            else return best_child(tree, EXPLORATION_COEFFICIENT);
        }
        else return child;
    }
}

static void default_policy(struct board *const board, int *const winner_out,
    unsigned int *const seed)
{
    const int move = random_move(board, seed);

    if (move < 0) *winner_out = board__winner(board);
    else
    {
        board__apply_move(board, move);
        default_policy(board, winner_out, seed);
    }
}

static void backup(struct tree *const tree,
    const int current_player, const int winner)
{
    tree->visits++;
    tree->wins += (int) (current_player == winner);
}

static void search1_rec(struct board *const board, struct tree *const tree,
    int *const winner_out, unsigned int *const seed)
{
    const int child = tree_policy(board, tree);

    if (child < 0) *winner_out = board__winner(board);
    else
    {
        const int current_player = board__current_player(board);
        board__apply_move(board, tree->children[child].move);
        if (tree->complete)
            search1_rec(board, &tree->children[child], winner_out, seed);
        else default_policy(board, winner_out, seed);
        backup(&tree->children[child], current_player, *winner_out);
    }
}

static void search1(struct board *const board, struct tree *const root,
    unsigned int *const seed)
{
    int winner;
    search1_rec(board, root, &winner, seed);
    root->visits++;  // technically we shouldn't count if children are empty, but that means game is over
}

struct search *search__alloc(struct board *const board)
{
    struct search *const search = calloc(1, sizeof (struct search));

    const unsigned int seed = (unsigned int) time(NULL) ^ (unsigned int) getpid();

    for (int i = 0; i < PARALLELISATION; i++)
        search->seed[i] = seed ^ (unsigned int) i;

    if (board == NULL)
    {
        search->board = malloc(board__size());
        board__reset(search->board);
    }
    else
        search->board = board;

    return search;
}

void search__search(struct search *const search, const double timeout)
{
    const double stop_time = omp_get_wtime() + timeout;

    const size_t board_size = board__size();

    struct board *board[PARALLELISATION];
    for (int i = 0; i < PARALLELISATION; i++)
        board[i] = malloc(board_size);

    #pragma omp parallel
    do  // guarantee at least one child
    {
        // nowait, schedule(static) effectively decouples all threads
        // from each other: each owns a subset of the trees, and
        // will iterate and stop at its own pace.
        #pragma omp for nowait, schedule(static)
        for (int i = 0; i < PARALLELISATION; i++)
        {
            memcpy(board[i], search->board, board_size);
            search1(board[i], &search->root[i], &search->seed[i]);
        }
    }
    while (omp_get_wtime() < stop_time);

    for (int i = 0; i < PARALLELISATION; i++)
        free(board[i]);
}

struct board *search__board(const struct search *const search)
{
    return search->board;
}

int search__best_move(const struct search *const search)
{
    int num_children = 0;
    for (int i = 0; i < PARALLELISATION; i++)
        if (search->root[i].num_children > num_children)
            num_children = search->root[i].num_children;
    assert(num_children > 0);

    int best = -1;
    float score = -INFINITY;

    for (int i = 0; i < num_children; i++)
    {
        long int visits = 0, wins = 0;
        int move = -1;

        for (int j = 0; j < PARALLELISATION; j++)
        {
            if (i < search->root[j].num_children)
            {
                assert(move == -1 || move == search->root[j].children[i].move);
                move = search->root[j].children[i].move;
                visits += search->root[j].children[i].visits;
                wins += search->root[j].children[i].wins;
            }
        }

        assert(visits > 0);

        const float new_score = (float) wins / (float) visits;

        if (new_score > score)
        {
            best = move;
            score = new_score;
        }
    }

    return best;
}

float search__win_chance(const struct search *const search, const int move)
{
    long int visits = 0, wins = 0;

    for (int i = 0; i < PARALLELISATION; i++)
    {
        for (int j = 0; j < search->root[i].num_children; j++)
        {
            if (search->root[i].children[j].move == move)
            {
                visits += search->root[i].children[j].visits;
                wins += search->root[i].children[j].wins;
                break;
            }
        }
    }

    if (visits > 0) return (float) wins / (float) visits;
    else return NAN;
}

void search__step(struct search *const search, const int move)
{
    for (int i = 0; i < PARALLELISATION; i++)
    {
        int j;
        for (j = 0; j < search->root[i].num_children; j++)
            if (search->root[i].children[j].move == move)
                break;

        if (j < search->root[i].num_children)
        {
            for (int k = 0; k < search->root[i].num_children; k++)
                if (k != j) free_tree(&search->root[i].children[k]);

            const struct tree new_root = search->root[i].children[j];
            free(search->root[i].children);
            search->root[i] = new_root;
        }
        else
        {
            free(search->root[i].children);
            memset(&search->root[i], 0, sizeof search->root[i]);
        }
    }

    board__apply_move(search->board, move);
}

void search__free(struct search *const search)
{
    for (int i = 0; i < PARALLELISATION; i++)
        free_tree(&search->root[i]);
    free(search->board);
    free(search);
}

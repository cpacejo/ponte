CFLAGS = -std=gnu11 -fopenmp -march=native -Wall -Werror -ffast-math -g -O3 -DNDEBUG $(EXTRA_CFLAGS)
LDLIBS = -lm $(EXTRA_LDLIBS)
LDFLAGS = -fopenmp $(EXTRA_LDFLAGS)
OBJS = board.o search.o test.o
.DEFAULT: test
test: $(OBJS)
.PHONY: clean
clean:
	rm -f $(OBJS) test

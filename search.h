#ifndef SEARCH_H
#define SEARCH_H

struct search;

struct search *search__alloc(struct board *);  // takes ownership of board if not NULL
void search__search(struct search *, double timeout);
struct board *search__board(const struct search *);
int search__best_move(const struct search *);
float search__win_chance(const struct search *, const int move);
void search__step(struct search *, const int move);
void search__free(struct search *);

void search__dump_stats(void);

#endif

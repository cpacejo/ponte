#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "board.h"

#define BOARD_WIDTH 10
#define BOARD_HEIGHT 10
#define MAX_BRIDGES 25
#define MAX_BRIDGE_LENGTH 3
#define MAX_BRIDGE_UNDER 2
#define ISLAND_SIZE 4

#define ASSUME(x) do { \
    assert(x); \
    if (!(x)) __builtin_unreachable(); \
} while (0)

enum bridge_orient
{
    ORIENT_2_0,
#define FIRST_ORIENT ORIENT_2_0
    ORIENT_2_1,
    ORIENT_2_2,
    ORIENT_1_2,
    ORIENT_0_2,
    ORIENT_N1_2,
    ORIENT_N2_2,
    ORIENT_N2_1
#define LAST_ORIENT ORIENT_N2_1
};

static const uint16_t bridge_pattern[][MAX_BRIDGE_LENGTH] = {
    [ORIENT_2_0] = { 0b011100 << (BOARD_WIDTH - 2), 0, 0 },
    [ORIENT_2_1] = { 0b001100 << (BOARD_WIDTH - 2), 0b011000 << (BOARD_WIDTH - 2), 0 },
    [ORIENT_2_2] = { 0b000100 << (BOARD_WIDTH - 2), 0b001000 << (BOARD_WIDTH - 2), 0b010000 << (BOARD_WIDTH - 2) },
    [ORIENT_1_2] = { 0b000100 << (BOARD_WIDTH - 2), 0b001100 << (BOARD_WIDTH - 2), 0b001000 << (BOARD_WIDTH - 2) },
    [ORIENT_0_2] = { 0b000100 << (BOARD_WIDTH - 2), 0b000100 << (BOARD_WIDTH - 2), 0b000100 << (BOARD_WIDTH - 2) },
    [ORIENT_N1_2] = { 0b000100 << (BOARD_WIDTH - 2), 0b000110 << (BOARD_WIDTH - 2), 0b000010 << (BOARD_WIDTH - 2) },
    [ORIENT_N2_2] = { 0b000100 << (BOARD_WIDTH - 2), 0b000010 << (BOARD_WIDTH - 2), 0b000001 << (BOARD_WIDTH - 2) },
    [ORIENT_N2_1] = { 0b000110 << (BOARD_WIDTH - 2), 0b000011 << (BOARD_WIDTH - 2), 0 }
};

static const struct { int dx, dy; } bridge_under[][MAX_BRIDGE_UNDER] = {
    [ORIENT_2_0] = { { 1, 0 }, { 1, 0 } },
    [ORIENT_2_1] = { { 1, 0 }, { 1, 1 } },
    [ORIENT_2_2] = { { 1, 1 }, { 1, 1 } },
    [ORIENT_1_2] = { { 1, 1 }, { 0, 1 } },
    [ORIENT_0_2] = { { 0, 1 }, { 0, 1 } },
    [ORIENT_N1_2] = { { 0, 1 }, { -1, 1 } },
    [ORIENT_N2_2] = { { -1, 1 }, { -1, 1 } },
    [ORIENT_N2_1] = { { -1, 1 }, { -1, 0 } }
};

static const struct { int dx, dy; } bridge_end[] = {
    [ORIENT_2_0] = { 2, 0 },
    [ORIENT_2_1] = { 2, 1 },
    [ORIENT_2_2] = { 2, 2 },
    [ORIENT_1_2] = { 1, 2 },
    [ORIENT_0_2] = { 0, 2 },
    [ORIENT_N1_2] = { -1, 2 },
    [ORIENT_N2_2] = { -2, 2 },
    [ORIENT_N2_1] = { -2, 1 }
};

struct board
{
    int turn;
    int prev_x1, prev_y1, prev_x2, prev_y2;
    bool placing_second_square;
    bool game_ending, swapped;
    // 1 sentinel on either side on both axes
    uint8_t space[BOARD_HEIGHT + 2][BOARD_WIDTH + 2];
    int num_bridges;
    // MAX_BRIDGE_LENGTH - 1 sentinels on + side only on both axes
    // TODO: maybe sentinels on both ends of X axis, to simplify shift?
    uint16_t has_bridge[BOARD_HEIGHT + MAX_BRIDGE_LENGTH - 1];

    // TODO: maybe also have is_occupied mask, to speed up move_is_valid?

    // scoring
    struct { uint16_t x: 4, y: 4, orient: 3; } bridge[MAX_BRIDGES];
    int islands[2];
};

#define MIN_PX 0
#define MAX_PX 3
#define MIN_PY (-4)
#define MAX_PY 3
#define MAX_SIZE 7

#define SPACE_OWNER_DELTA(owner) ((owner) << 1)
#define SPACE(owner, px, py) ({ \
    assert((px) <= MAX_PX); assert((px) >= MIN_PX); \
    assert((py) <= MAX_PY); assert((py) >= MIN_PY); \
    ((((py) + 4) << 4) | ((px) << 2) | SPACE_OWNER_DELTA(owner) | 1); \
})
#define SPACE_REP_SIZE_DELTA(size) ((size) << 2)
// rep is min (x, y) of group; so px is always nonnegative
// TODO: is this invariant actually useful?
#define SPACE_REP(owner, size) ({ \
    assert((size) <= MAX_SIZE); \
    (SPACE_REP_SIZE_DELTA(size) | SPACE_OWNER_DELTA(owner)); \
})
#define SPACE_OWNER(s) (((s) >> 1) & 1)
#define SPACE_IS_REP(s) (((s) & 1) == 0)
#define SPACE_SIZE(s) (((s) >> 2) & 7)
#define SPACE_PX(s) (((s) >> 2) & 3)
#define SPACE_PY(s) ((((s) >> 4) & 7) - 4)

static bool space_is_owned(const struct board *const board, const int owner, const int x, const int y)
{
    return board->space[y+1][x+1] != 0 && SPACE_OWNER(board->space[y+1][x+1]) == owner;
}

static __attribute__ ((hot))
void space_rep(struct board *const board, const int x, const int y,
    int *const rep_x, int *const rep_y)
{
    if (SPACE_IS_REP(board->space[y+1][x+1]))
    {
        *rep_x = x;
        *rep_y = y;
    }
    else
    {
        space_rep(board, x - SPACE_PX(board->space[y+1][x+1]), y - SPACE_PY(board->space[y+1][x+1]), rep_x, rep_y);
        board->space[y+1][x+1] = SPACE(SPACE_OWNER(board->space[y+1][x+1]), x - *rep_x, y - *rep_y);
    }
}

static __attribute__ ((hot))
int space_size_if_owned(struct board *const board, const int owner, const int x, const int y)
{
    if (SPACE_OWNER(board->space[y+1][x+1]) == owner)
    {
        int rep_x, rep_y;
        space_rep(board, x, y, &rep_x, &rep_y);
        return SPACE_SIZE(board->space[rep_y+1][rep_x+1]);
    }
    else return 0;
}

static __attribute__ ((hot))
int space_size_if_not(struct board *const board,
    const int x, const int y, const int not_x, const int not_y)
{
    int rep_x, rep_y, rep_not_x, rep_not_y;
    space_rep(board, x, y, &rep_x, &rep_y);
    space_rep(board, not_x, not_y, &rep_not_x, &rep_not_y);
    if (rep_x == rep_not_x && rep_y == rep_not_y) return 0;
    else return SPACE_SIZE(board->space[rep_y+1][rep_x+1]);
}

static void space_join(struct board *const board, const int owner, const int x1, const int y1, const int x2, const int y2)
{
    int rep_x1, rep_y1, rep_x2, rep_y2;
    space_rep(board, x1, y1, &rep_x1, &rep_y1);
    space_rep(board, x2, y2, &rep_x2, &rep_y2);

    assert(SPACE_OWNER(board->space[rep_y1+1][rep_x1+1]) == owner);
    assert(SPACE_OWNER(board->space[rep_y2+1][rep_x2+1]) == owner);

    if (rep_x1 == rep_x2 && rep_y1 == rep_y2) return;

    // FIXME: Is ordering actually useful?  Maybe order by size instead?
    if (rep_x1 < rep_x2 || (rep_x1 == rep_x2 && rep_y1 < rep_y2))
    {
        board->space[rep_y1+1][rep_x1+1] += SPACE_REP_SIZE_DELTA(SPACE_SIZE(board->space[rep_y2+1][rep_x2+1]));
        board->space[rep_y2+1][rep_x2+1] = SPACE(owner, rep_x2 - rep_x1, rep_y2 - rep_y1);
        if (SPACE_SIZE(board->space[rep_y1+1][rep_x1+1]) >= ISLAND_SIZE)
            board->islands[owner]++;
    }
    else
    {
        board->space[rep_y2+1][rep_x2+1] += SPACE_REP_SIZE_DELTA(SPACE_SIZE(board->space[rep_y1+1][rep_x1+1]));
        board->space[rep_y1+1][rep_x1+1] = SPACE(owner, rep_x1 - rep_x2, rep_y1 - rep_y2);
        if (SPACE_SIZE(board->space[rep_y2+1][rep_x2+1]) >= ISLAND_SIZE)
            board->islands[owner]++;
    }
}

static void space_add(struct board *const board, const int owner, const int x, const int y)
{
    assert(board->space[y+1][x+1] == 0);

    // Add us.
    board->space[y+1][x+1] = SPACE_REP(owner, 1);

    // Join with neighbors.
    if (space_is_owned(board, owner, x+1, y)) space_join(board, owner, x, y, x+1, y);
    if (space_is_owned(board, owner, x-1, y)) space_join(board, owner, x, y, x-1, y);
    if (space_is_owned(board, owner, x, y+1)) space_join(board, owner, x, y, x, y+1);
    if (space_is_owned(board, owner, x, y-1)) space_join(board, owner, x, y, x, y-1);
}

size_t board__size(void)
{
    return sizeof (struct board);
}

void board__reset(struct board *const board)
{
    memset(board, 0, sizeof (struct board));

    // set up sentinels
    for (int y = 0; y < BOARD_HEIGHT; y++)
        board->has_bridge[y] = (uint16_t) (~0 << BOARD_WIDTH);

    for (int y = BOARD_HEIGHT; y < BOARD_HEIGHT + MAX_BRIDGE_LENGTH - 1; y++)
        board->has_bridge[y] = (uint16_t) ~0;
}

int board__players(const struct board *const board)
{
    return 2;
}

static void count_score(struct board *const board, int *const score)
{
    int num_islands = 0;
    struct { int x, y, rep, count; } islands[MAX_BRIDGES * 2];

    for (int i = 0; i < board->num_bridges; i++)
    {
        int rep1_x, rep1_y, rep2_x, rep2_y;
        space_rep(board, board->bridge[i].x, board->bridge[i].y, &rep1_x, &rep1_y);
        space_rep(board, board->bridge[i].x + bridge_end[board->bridge[i].orient].dx,
            board->bridge[i].y + bridge_end[board->bridge[i].orient].dy, &rep2_x, &rep2_y);

        int rep1;
        for (rep1 = 0; rep1 < num_islands; rep1++)
            if (islands[rep1].x == rep1_x && islands[rep1].y == rep1_y)
                break;

        if (rep1 == num_islands)
        {
            num_islands++;
            islands[rep1].x = rep1_x;
            islands[rep1].y = rep1_y;
            islands[rep1].rep = rep1;
            islands[rep1].count = (int) SPACE_SIZE(board->space[rep1_y+1][rep1_x+1]) >= ISLAND_SIZE;
        }

        while (islands[rep1].rep != rep1)
            rep1 = islands[rep1].rep;

        int rep2;
        for (rep2 = 0; rep2 < num_islands; rep2++)
            if (islands[rep2].x == rep2_x && islands[rep2].y == rep2_y)
                break;

        if (rep2 == num_islands)
        {
            num_islands++;
            islands[rep2].x = rep2_x;
            islands[rep2].y = rep2_y;
            islands[rep2].rep = rep2;
            islands[rep2].count = (int) SPACE_SIZE(board->space[rep2_y+1][rep2_x+1]) >= ISLAND_SIZE;
        }

        while (islands[rep2].rep != rep2)
            rep2 = islands[rep2].rep;

        if (rep1 != rep2)
        {
            if (islands[rep1].count < islands[rep2].count)
            {
                islands[rep1].rep = rep2;
                islands[rep2].count += islands[rep1].count;
            }
            else
            {
                islands[rep2].rep = rep1;
                islands[rep1].count += islands[rep2].count;
            }
        }
    }

    score[0] = board->islands[0];
    score[1] = board->islands[1];

    for (int i = 0; i < num_islands; i++)
    {
        const int owner = SPACE_OWNER(board->space[islands[i].y+1][islands[i].x+1]);
        if (islands[i].rep == i)
            score[owner] += (islands[i].count * (islands[i].count - 1)) / 2;
    }
}

static void count_bridges(const struct board *const board, int *const bridges)
{
    bridges[0] = 0;
    bridges[1] = 0;
    for (int i = 0; i < board->num_bridges; i++)
        bridges[SPACE_OWNER(board->space[board->bridge[i].y+1][board->bridge[i].x+1])]++;
}

int board__winner(struct board *const board)
{
    int score[2];
    count_score(board, score);

    if (score[0] > score[1]) return 0;
    else if (score[1] > score[0]) return 1;
    else
    {
        if (board->islands[0] > board->islands[1]) return 0;
        else if (board->islands[1] > board->islands[0]) return 1;
        else
        {
            int bridges[2];
            count_bridges(board, bridges);
            if (bridges[0] > bridges[1]) return 0;
            else if (bridges[1] > bridges[0]) return 1;
            else return -1;
        }
    }
}

static int current_color(const struct board *const board)
{
    return board->turn & 1;
}

int board__current_player(const struct board *const board)
{
    return current_color(board);
}

#define MOVE(x, y) ((x) | ((y) << 4))
#define MOVE_BRIDGE(x, y, orient) ((x) | ((y) << 4) | (1 << 8) | (((int) (orient) << 9)))
#define MOVE_PASS (1 << 12)
#define MOVE_X(m) ((m) & 0xF)
#define MOVE_Y(m) (((m) >> 4) & 0xF)
#define MOVE_IS_BRIDGE(m) ((bool) (((m) >> 8) & 1))
#define MOVE_BRIDGE_ORIENT(m) ((enum bridge_orient) (((m) >> 9) & 0x7))
#define MOVE_IS_PASS(m) ((m) >= MOVE_PASS)

static bool is_diag_touching(const struct board *const board,
    const int owner, const int x, const int y, const int dx, const int dy)
{
    ASSUME(x >= 0 && x < BOARD_WIDTH);
    ASSUME(y >= 0 && y < BOARD_HEIGHT);
    ASSUME(dx >= -1 && dx <= 1);
    ASSUME(dy >= -1 && dy <= 1);

    // check forward-left and forward-right;
    // but only if no square adjacent to that and us

    if (!space_is_owned(board, owner, x+dx, y+dy) &&
        !space_is_owned(board, owner, x+dy, y-dx) &&
        space_is_owned(board, owner, x+dx+dy, y-dx+dy))
        return true;

    if (!space_is_owned(board, owner, x+dx, y+dy) &&
        !space_is_owned(board, owner, x-dy, y+dx) &&
        space_is_owned(board, owner, x+dx-dy, y+dx+dy))
        return true;

    // recur on adjacent squares
    // permissible geometry of islands with one missing square
    // ensures that we won't loop or have false positives

    if (space_is_owned(board, owner, x+dx, y+dy) &&
        is_diag_touching(board, owner, x+dx, y+dy, dx, dy))
        return true;

    if (space_is_owned(board, owner, x+dy, y-dx) &&
        is_diag_touching(board, owner, x+dy, y-dx, dy, -dx))
        return true;

    if (space_is_owned(board, owner, x-dy, y+dx) &&
        is_diag_touching(board, owner, x-dy, y+dx, -dy, dx))
        return true;

    return false;
}

static __attribute__ ((hot))
bool move_is_valid(struct board *const board, const int x, const int y)
{
    const int owner = current_color(board);

    // space must be empty
    if (board->space[y+1][x+1] != 0) return false;

    // move must not be under bridge
    if (((board->has_bridge[y] >> x) & 1) != 0) return false;

    bool own_n = space_is_owned(board, owner, x, y+1);
    bool own_e = space_is_owned(board, owner, x+1, y);
    bool own_s = space_is_owned(board, owner, x, y-1);
    bool own_w = space_is_owned(board, owner, x-1, y);

    // move must not make > island
    const int size_n = own_n ? space_size_if_not(board, x, y+1, x-1, y) : 0;
    const int size_e = own_e ? space_size_if_not(board, x+1, y, x, y+1) : 0;
    const int size_s = own_s ? space_size_if_not(board, x, y-1, x+1, y) : 0;
    const int size_w = own_w ? space_size_if_not(board, x-1, y, x, y-1) : 0;
    const int total_size = size_n + size_s + size_e + size_w + 1;
    if (total_size > ISLAND_SIZE) return false;

    // If move makes island, must not touch other island or sand bank diagonally.
    if (total_size >= ISLAND_SIZE)
    {
        // Can't turn a sand bank that diagonally touches another sand bank
        // (that isn't touching us) into an island.
        if (own_n && is_diag_touching(board, owner, x, y+1, 0, 1)) return false;
        if (own_e && is_diag_touching(board, owner, x+1, y, 1, 0)) return false;
        if (own_s && is_diag_touching(board, owner, x, y-1, 0, -1)) return false;
        if (own_w && is_diag_touching(board, owner, x-1, y, -1, 0)) return false;

        // Can't diagonally touch another sand bank unless it will become part of us.
        if (!own_e && !own_n && space_is_owned(board, owner, x+1, y+1)) return false;
        if (!own_e && !own_s && space_is_owned(board, owner, x+1, y-1)) return false;
        if (!own_w && !own_s && space_is_owned(board, owner, x-1, y-1)) return false;
        if (!own_w && !own_n && space_is_owned(board, owner, x-1, y+1)) return false;
    }
    else
    {
        // Else, must not touch island diagonally.
        // We don't need to skip squares which would be joined to us,
        // since by definition their size is less than ISLAND_SIZE.
        if (space_size_if_owned(board, owner, x+1, y+1) >= ISLAND_SIZE) return false;
        if (space_size_if_owned(board, owner, x+1, y-1) >= ISLAND_SIZE) return false;
        if (space_size_if_owned(board, owner, x-1, y-1) >= ISLAND_SIZE) return false;
        if (space_size_if_owned(board, owner, x-1, y+1) >= ISLAND_SIZE) return false;
    }

    return true;
}

static __attribute__ ((hot))
bool bridge_move_any_is_valid(struct board *const board, const int x, const int y)
{
    ASSUME(x >= 0 && x < BOARD_WIDTH);
    ASSUME(y >= 0 && y < BOARD_HEIGHT);

    const int owner = current_color(board);

    return space_is_owned(board, owner, x, y) &&
        ((board->has_bridge[y] >> x) & 1) == 0;
}

static __attribute__ ((hot))
bool bridge_move_is_valid(struct board *const board, const int x, const int y, const enum bridge_orient orient)
{
    ASSUME(x >= 0 && x < BOARD_WIDTH);
    ASSUME(y >= 0 && y < BOARD_HEIGHT);

    const int owner = current_color(board);

    // both ends must be owned by player
    assert(space_is_owned(board, owner, x, y));
    if (!space_is_owned(board, owner,
        x + bridge_end[orient].dx, y + bridge_end[orient].dy)) return false;

    // must have no pieces beneath
    for (int i = 0; i < MAX_BRIDGE_UNDER; i++)
        if (board->space[y + bridge_under[orient][i].dy + 1][x + bridge_under[orient][i].dx + 1] != 0)
            return false;

    // must not be off edge
    // must have no other bridge on space
    // must have no other bridge crossing
    for (int i = 0; i < MAX_BRIDGE_LENGTH; i++)
        if ((board->has_bridge[y + i] & (bridge_pattern[orient][i] >> (BOARD_WIDTH - x))) != 0)
            return false;

    return true;
}

static bool move_can_be_completed(const struct board *const board, const int move)
{
    // FIXME: this is rather inefficient, but is there really a better way?

    assert(!MOVE_IS_BRIDGE(move) && !MOVE_IS_PASS(move));
    assert(!board->placing_second_square);

    struct board board2 = *board;
    board__apply_move(&board2, move);
    // can't start with -1 b/c that performs this check again
    return move_is_valid(&board2, 0, 0) || board__next_move(&board2, MOVE(0, 0)) >= 0;
}

int board__next_move(struct board *const board, int move)
{
    const bool init = move < 0;

    if (move < 0)
    {
        if (board->game_ending && current_color(board) == (int) board->swapped)
            return -1;  // game is over

        move = MOVE(0, 0);
        if (move_is_valid(board, 0, 0) && (board->placing_second_square || move_can_be_completed(board, move)))
            return move;
    }

    if (!MOVE_IS_BRIDGE(move) && !MOVE_IS_PASS(move))
    {
        int x = MOVE_X(move), y = MOVE_Y(move);
        for (;;)
        {
            x++;
            if (x >= BOARD_WIDTH)
            {
                x = 0;
                y++;
                if (y >= BOARD_HEIGHT) break;
            }

            if (move_is_valid(board, x, y) && (board->placing_second_square || move_can_be_completed(board, MOVE(x, y))))
                return MOVE(x, y);
        }

        if (board->placing_second_square)
        {
            // Shouldn't run out of moves when placing second square.
            assert(!init);
            return -1;
        }

        move = MOVE_PASS;

        // Can only pass if there is nowhere to place a piece (last turn),
        // or on the 2nd player's first turn.
        if (init || board->turn == 1)
            return move;
    }

    // Can't play bridge if we already placed a square this turn.
    assert(!board->placing_second_square);

    if (MOVE_IS_PASS(move))
    {
        move = MOVE_BRIDGE(0, 0, FIRST_ORIENT);
        if (bridge_move_any_is_valid(board, 0, 0) &&
            bridge_move_is_valid(board, 0, 0, FIRST_ORIENT))
            return move;
    }

    assert(MOVE_IS_BRIDGE(move));

    int x = MOVE_X(move), y = MOVE_Y(move);
    enum bridge_orient orient = MOVE_BRIDGE_ORIENT(move);

    if (!bridge_move_any_is_valid(board, x, y))
        orient = LAST_ORIENT;

    for (;;)
    {
        if (orient >= LAST_ORIENT)
        {
            x++;
            if (x >= BOARD_WIDTH)
            {
                x = 0;
                y++;
                if (y >= BOARD_HEIGHT) break;
            }

            if (!bridge_move_any_is_valid(board, x, y))
                continue;

            orient = FIRST_ORIENT;
        }
        else
            orient = (enum bridge_orient) ((int) orient + 1);

        if (bridge_move_is_valid(board, x, y, orient))
            return MOVE_BRIDGE(x, y, orient);
    }

    return -1;
}

int board__random_move(struct board *const board, const long int random)
{
    const bool is_bridge = random % 2;
    const int x = (random / 2) % BOARD_WIDTH;
    const int y = (random / (2 * BOARD_WIDTH)) % BOARD_HEIGHT;
    const enum bridge_orient orient =
        (int) FIRST_ORIENT +
        (random / (2 * BOARD_WIDTH * BOARD_HEIGHT)) % ((int) LAST_ORIENT - (int) FIRST_ORIENT + 1);

    int move;

    if (is_bridge && !board->placing_second_square)
    {
        if (board->turn == 1) return MOVE_PASS;  // special case!

        move = MOVE_BRIDGE(x, y, orient);
        if (bridge_move_any_is_valid(board, x, y) && bridge_move_is_valid(board, x, y, orient))
            return move;
    }
    else
    {
        move = MOVE(x, y);
        if (move_is_valid(board, x, y) && (board->placing_second_square || move_can_be_completed(board, move)))
            return move;

        // We assume that MOVE_PASS is checked/generated after square-placing moves.
        // This way, if no moves are available, that will result in a pass below.
    }

    move = board__next_move(board, move);
    if (move >= 0) return move;
    else return board__next_move(board, -1);
}

void board__apply_move(struct board *const board, const int move)
{
    assert(move >= 0);

    if (MOVE_IS_PASS(move))
    {
        assert(!board->placing_second_square);
        if (board->turn == 1)
        {
            // 2nd player chose to play 1st color
            board->space[board->prev_y1+1][board->prev_x1+1] |= SPACE_OWNER_DELTA(1);
            board->space[board->prev_y2+1][board->prev_x2+1] |= SPACE_OWNER_DELTA(1);
            board->swapped = true;
        }
        else
        {
            // pass at end of game
            board->game_ending = true;
        }
        board->turn++;
    }
    else if (MOVE_IS_BRIDGE(move))
    {
        ASSUME(board->num_bridges < MAX_BRIDGES);
        board->bridge[board->num_bridges].x = MOVE_X(move);
        board->bridge[board->num_bridges].y = MOVE_Y(move);
        board->bridge[board->num_bridges].orient = MOVE_BRIDGE_ORIENT(move);
        board->num_bridges++;

        for (int i = 0; i < MAX_BRIDGE_LENGTH; i++)
            board->has_bridge[MOVE_Y(move)+i] |=
                bridge_pattern[MOVE_BRIDGE_ORIENT(move)][i] >> (BOARD_WIDTH - MOVE_X(move));

        assert(!board->placing_second_square);
        board->turn++;
    }
    else
    {
        space_add(board, current_color(board), MOVE_X(move), MOVE_Y(move));

        if (board->placing_second_square)
        {
            board->prev_x2 = MOVE_X(move);
            board->prev_y2 = MOVE_Y(move);
            board->turn++;
        }
        else
        {
            board->prev_x1 = MOVE_X(move);
            board->prev_y1 = MOVE_Y(move);
        }

        board->placing_second_square = !board->placing_second_square;
    }
}

bool board__move_is_legal(struct board *const board, const int move)
{
    if (MOVE_IS_PASS(move))
    {
        // We assume that square-placing moves are ordered first, so
        // if we are allowed to pass (due to not being able to place)
        // MOVE_PASS will be returned as the first move.
        return board->turn == 1 || board__next_move(board, -1) == MOVE_PASS;
    }
    else if (MOVE_IS_BRIDGE(move))
    {
        return
            bridge_move_any_is_valid(board, MOVE_X(move), MOVE_Y(move)) &&
            bridge_move_is_valid(board, MOVE_X(move), MOVE_Y(move), MOVE_BRIDGE_ORIENT(move));
    }
    else
        return move_is_valid(board, MOVE_X(move), MOVE_Y(move)) &&
            (board->placing_second_square || move_can_be_completed(board, move));
}

#include <ctype.h>
#include <stdio.h>

int board__print_board(FILE *file, struct board *const board)
{
    char grid[BOARD_HEIGHT][BOARD_WIDTH];

    for (int y = 0; y < BOARD_HEIGHT; y++)
    {
        for (int x = 0; x < BOARD_WIDTH; x++)
        {
            if (((board->has_bridge[y] >> x) & 1) != 0)
            {
                if (board->space[y+1][x+1] == 0)
                    grid[y][x] = '=';
                else
                {
                    switch (SPACE_OWNER(board->space[y+1][x+1]))
                    {
                        case 0: grid[y][x] = 'O'; break;
                        case 1: grid[y][x] = 'X'; break;
                        default: grid[y][x] = '?'; break;
                    }
                }
            }
            else
            {
                if (board->space[y+1][x+1] == 0)
                    grid[y][x] = '.';
                else
                {
                    switch (SPACE_OWNER(board->space[y+1][x+1]))
                    {
                        case 0: grid[y][x] = 'o'; break;
                        case 1: grid[y][x] = 'x'; break;
                        default: grid[y][x] = '?'; break;
                    }
                }
            }
        }
    }

    for (int y = BOARD_HEIGHT - 1; y >= 0; y--)
        if (fprintf(file, " %2i %.*s\n", y + 1, BOARD_WIDTH, grid[y]) < 0)
            return -1;

    if (fprintf(file, "\n    %.*s\n", BOARD_WIDTH, "abcdefghijklmnopqrstuvwxyz") < 0)
        return -1;

    int score[2], bridges[2];
    count_score((struct board *const) board, score);
    count_bridges((struct board *const) board, bridges);
    fprintf(file, "%i %i / %i %i / %i %i\n",
        score[0], score[1], board->islands[0], board->islands[1], bridges[0], bridges[1]);

    if (board->game_ending)
    {
        if (current_color(board) == (int) board->swapped)
        {
            const int winner = board__winner(board);
            if (fprintf(file, "\nGame over; %s.\n",
                    winner == 0 ? "O won" :
                    winner == 1 ? "X won" : "tie") < 0)
                return -1;
            
        }
        else
        {
            if (fprintf(file, "\n%s to play final turn.\n",
                    current_color(board) == 0 ? "O" : "X") < 0)
                return -1;
        }
    }
    else
    {
        if (fprintf(file, "\n%s to play.\n", current_color(board) == 0 ? "O" : "X") < 0)
            return -1;
    }

    return 0;
}

int board__print_move(FILE *const file, const int move)
{
    if (MOVE_IS_PASS(move))
        return fprintf(file, "pass\n");
    else if (MOVE_IS_BRIDGE(move))
        return fprintf(file, "%c%i=%c%i\n",
            'a' + MOVE_X(move), MOVE_Y(move) + 1,
            'a' + MOVE_X(move) + bridge_end[MOVE_BRIDGE_ORIENT(move)].dx,
            MOVE_Y(move) + bridge_end[MOVE_BRIDGE_ORIENT(move)].dy + 1);
    else
        return fprintf(file, "%c%i\n",
            'a' + MOVE_X(move), MOVE_Y(move) + 1);
}

int board__parse_move(const char *const line)
{
    char col1c, col2c;
    int row1, row2, col1, col2;

    if (sscanf(line, "%c%i=%c%i", &col1c, &row1, &col2c, &row2) >= 4)
    {
        col1 = tolower(col1c) - 'a' + 1;
        col2 = tolower(col2c) - 'a' + 1;

        if (col1 >= 1 && col1 <= BOARD_WIDTH &&
            row1 >= 1 && row1 <= BOARD_HEIGHT &&
            col2 >= 1 && col2 <= BOARD_WIDTH &&
            row2 >= 1 && row2 <= BOARD_HEIGHT)
        {
            int x, y, dx, dy;

            if (row1 <= row2)
            {
                x = col1-1; y = row1-1;
                dx = col2 - col1; dy = row2 - row1;
            }
            else
            {
                x = col2-1; y = row2-1;
                dx = col1 - col2; dy = row1 - row2;
            }

            enum bridge_orient orient = FIRST_ORIENT;
            for (;;)
            {
                if (bridge_end[orient].dx == dx &&
                    bridge_end[orient].dy == dy)
                    break;
                if (orient == LAST_ORIENT) return -1;
                orient = (enum bridge_orient) ((int) orient + 1);
            }

            return MOVE_BRIDGE(x, y, orient);
        }
    }

    if (sscanf(line, "%c%i", &col1c, &row1) >= 2)
    {
        col1 = tolower(col1c) - 'a' + 1;

        if (col1 >= 1 && col1 <= BOARD_WIDTH &&
            row1 >= 1 && row1 <= BOARD_HEIGHT)
            return MOVE(col1-1, row1-1);
    }

    if (strcmp(line, "pass") == 0)
        return MOVE_PASS;

    return -1;
}
